
#Calendar

##Summary

This is just a simple calendar implementation I made in 2008, which nevertheless still works in modern browsers.

As the code is ancient and outdated, I'm releasing it into the public domain.

##Demo

For a live demo, go to [http://jslegers.github.com/calendar/](http://jslegers.github.com/calendar/).